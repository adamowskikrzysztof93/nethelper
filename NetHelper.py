import psutil
import sys
import argparse

parser = argparse.ArgumentParser(description="Check connection count.")
g = parser.add_mutually_exclusive_group()
g.add_argument('-p', '--port', type=int, help="Specify port")
g.add_argument('-n', '--process-name', help="Specify process name")

args = parser.parse_args()
exit_code = 0
exit_message = ""
command_type = ""

if args.port is not None:
    conn=[net for net in psutil.net_connections() if net.laddr.port == args.port and net.status == 'LISTEN']
    if conn:
        print(f"listened on port: {args.port}")
        print(psutil.Process(conn[0].pid))
    else:
        print("Port not found")
elif args.process_name is not None:
    for proc in psutil.process_iter(['pid', 'name', 'username']):
        process_name = str(proc.__dict__['_name']).lower()
        pid = proc.__dict__['_pid']
        if args.process_name.lower() in process_name:
            conn=[net for net in psutil.net_connections() if net.pid == pid and net.status == 'LISTEN']
            for c in conn:
                print(f'{process_name}, pid: {pid} listened on address: {c.laddr.ip}  on port: {c.laddr.port}')

else:
    print("Please provide agruments or type -h")